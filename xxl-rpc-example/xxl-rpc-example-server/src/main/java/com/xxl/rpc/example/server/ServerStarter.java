package com.xxl.rpc.example.server;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author yangzhigang
 *
 */
public class ServerStarter {

	private static final String[] CONFIG_RESOUCES = new String[] { "applicationcontext-rpc-provider.xml" };

	private static ClassPathXmlApplicationContext springContext = null;

	private static volatile boolean running = true;

	public static void main(String[] args) {

		startSpringContainer();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if (springContext != null) {
					springContext.stop();
					springContext.close();
					springContext = null;
				}
				synchronized (ServerStarter.class) {
					running = false;
					ServerStarter.class.notify();
				}
			}
		});

		synchronized (ServerStarter.class) {
			while (running) {
				try {
					ServerStarter.class.wait();
				} catch (Throwable e) {
				}
			}
		}

	}

	private static void startSpringContainer() {
		springContext = new ClassPathXmlApplicationContext(CONFIG_RESOUCES);
		springContext.start();
		System.out.println("application start success......");
	}
}
